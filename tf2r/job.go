package tf2r

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

func inr(a uint8) uint8 {
	return uint8((float64(a) / 255) * 5)
}

type RGB [3]uint8

func (r RGB) To256() uint8 {
	return 16 + (inr(r[0]) * 36) + (inr(r[1]) * 6) + inr(r[2])
}

func (r RGB) MarshalText() (text []byte, err error) {
	return []byte(fmt.Sprintf("%#X%#X%#X", r[0], r[1], r[2])), nil
}

func (r RGB) String() string {
	t, _ := r.MarshalText()
	return "#" + string(t)
}

func (r *RGB) UnmarshalText(text []byte) (err error) {
	n, err := fmt.Fscanf(bytes.NewReader(text), "%2x%2x%2x", &r[0], &r[1], &r[2])
	if err != nil {
		return
	}

	if n < 3 {
		err = fmt.Errorf("string %+q is an invalid RGB hex code", text)
	}
	return
}

type Movement uint8

const (
	NoMovement Movement = iota
	Joined
	Left
)

func (m Movement) String() (s string) {
	switch m {
	case NoMovement:
		s = "None"
	case Joined:
		s = "Joined"
	case Left:
		s = "Left"
	}
	return
}

type (
	Message struct {
		Id      int `json:"id,string"`
		Name    string
		Profile string
		Color   RGB
		Avatar  string
		Value   string `json:"message"`
		Self    bool   `json:"-"`

		Movement Movement `json:"-"`
	}

	User struct {
		Color   RGB
		Id      int `json:",string"`
		Name    string
		Profile string
	}

	Chat struct {
		Lastid int `json:"chatmax,string"`
		Users  []User
		Chat   []Message
	}

	Response struct {
		Status string
	}
)

func (u User) Message() Message {
	return Message{
		Id:      u.Id,
		Name:    u.Name,
		Profile: u.Profile,
		Color:   u.Color,
	}
}

func (r Response) Error() (err error) {
	if r.Status != "ok" {
		err = errors.New("status: " + r.Status)
	}
	return
}

type Conn struct {
	session  string
	messages chan<- Message
	errors   chan<- error
	delay    time.Duration

	users map[int]User
	ulock sync.Mutex

	lastmsg int
	lmlock  sync.RWMutex

	close chan struct{}
}

func (c Conn) Say(s string) (err error) {

	r, err := c.Job(url.Values{
		"chat": {"true"},
		"mess": {s},
	})

	if err != nil {
		return
	}

	var rp struct {
		Response
		Message Message
	}

	if err = jsonInto(r, &rp); err != nil {
		return
	}

	if err = rp.Response.Error(); err != nil {
		return
	}

	rp.Message.Self = true
	go c.sendMessage(rp.Message)
	return

}

func jsonInto(r *http.Response, i interface{}) (err error) {
	defer r.Body.Close()

	bt, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(bt, i)
	return

}

func (c *Conn) Job(params url.Values) (r *http.Response, err error) {
	rq, err := http.NewRequest("POST", "http://tf2r.com/job.php", strings.NewReader(params.Encode()))
	if err != nil {
		return
	}

	rq.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	rq.AddCookie(&http.Cookie{Name: "session", Value: c.session})
	return http.DefaultClient.Do(rq)
}

func (c *Conn) lastMessage() int { c.lmlock.RLock(); defer c.lmlock.RUnlock(); return c.lastmsg }
func (c *Conn) setLastMessage(i int) {
	c.lmlock.Lock()
	defer c.lmlock.Unlock()
	if i == 1 {
		panic(i)
	}
	c.lastmsg = i
}

func (c *Conn) sendMessage(m Message) (success bool) {
	select {
	case <-c.close:
	case c.messages <- m:
		success = true
	}
	return
}

func (c *Conn) sendError(e error) {
	select {
	case <-c.close:
	case c.errors <- e:
	}
}

func (c *Conn) updateUsers(us []User) {
	c.ulock.Lock()
	defer c.ulock.Unlock()

	for _, v := range us {
		if _, ok := c.users[v.Id]; !ok {
			//a user has joined
			m := v.Message()
			m.Movement = Joined
			c.sendMessage(m)
			c.users[v.Id] = v
		}
	}

	if len(us) < len(c.users) {

	MapIteration:
		for k, u := range c.users {
			for _, v := range us {
				if k == v.Id {
					continue MapIteration
				}
			}

			m := u.Message()
			m.Movement = Left
			//user never found
			c.sendMessage(m)
			delete(c.users, k)
		}
	}
}

var getlastmsg = regexp.MustCompile("var lastcm \\= (\\d+);")

func (c *Conn) Poll(delay time.Duration, errors chan<- error) (err error) {
	if c.delay != 0 {
		panic("Call to Poll while polling!")
	}

	c.delay = delay
	c.errors = errors
	d := time.Tick(c.delay)

	//grab the chat page
	rq, err := http.NewRequest("GET", "http://tf2r.com/chat.html", nil)
	if err != nil {
		return
	}

	//add cookie
	rq.AddCookie(&http.Cookie{Name: "session", Value: c.session})

	rs, err := http.DefaultClient.Do(rq)
	if err != nil {
		return
	}
	defer rs.Body.Close()

	bt, err := ioutil.ReadAll(rs.Body)
	if err != nil {
		return
	}

	lm, err := strconv.Atoi(string(getlastmsg.FindSubmatch(bt)[1]))
	if err != nil {
		return
	}

	c.setLastMessage(lm)

	go func() {
		for {
			select {
			case <-c.close:
				return
			case <-d:
				lm := c.lastMessage()
				r, e := c.Job(url.Values{
					"chchat": {"true"},
					"lastm":  {strconv.Itoa(lm)},
				})

				if e != nil {
					c.errors <- e
					continue
				}

				var res struct {
					Response
					Chat Chat `json:"message"`
				}

				e = jsonInto(r, &res)
				if e != nil {
					c.errors <- e
					continue
				}

				if e = res.Response.Error(); e != nil {
					c.errors <- e
					continue
				}

				c.setLastMessage(res.Chat.Lastid)

				c.updateUsers(res.Chat.Users)

				//prevent flood of old messages
				if lm != 0 {
					for _, v := range res.Chat.Chat {
						c.messages <- v
					}
				}

			}
		}
	}()
	return
}

func NewConn(session string, messages chan<- Message) (c *Conn) {
	c = &Conn{
		messages: messages,
		session:  session,
		users:    make(map[int]User),
		close:    make(chan struct{}),
	}

	return
}

func (c Conn) Close() {
	close(c.close)
	close(c.messages)
}
