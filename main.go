package main

import (
	"bitbucket.org/zemnmez/tf2rcli/tf2r"
	"bufio"
	"code.google.com/p/go.crypto/ssh/terminal"
	"flag"
	"fmt"
	"html"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

var cookie string

var badlinksre = regexp.MustCompile(`<a href="([^"]+)" >([^<]*)<\/a>`)

func replLinks(s string) string {
	return html.UnescapeString(badlinksre.ReplaceAllString(s, "$2"))
}

func formatColour(s string, colour tf2r.RGB) string {
	if runtime.GOOS == "linux" {
		return "\x1b[38;5;" + strconv.Itoa(int(colour.To256())) + "m" + s + "\x1b[0m"
	} else {
		return s
	}
}

func main() {
	flag.StringVar(&cookie, "cookie", "", "TF2R cookie (run `document.cookie.match(/session=([^;]+)/)[1]`)")
	flag.Parse()

	if cookie == "" {
		panic("No cookie provided")
	}

	//put terminal into raw mode
	oldState, err := terminal.MakeRaw(0)
	if err != nil {
		panic(err)
	}

	fmt.Println("Press escape to exit.")

	defer terminal.Restore(0, oldState)

	msg := make(chan tf2r.Message, 100)
	c := tf2r.NewConn(cookie, msg)

	errors := make(chan error)
	err = c.Poll(3*time.Second, errors)
	if err != nil {
		panic(err)
	}

	go func() {
		panic(<-errors)
	}()

	var s string
	var slock sync.RWMutex

	replaceLine := func(s string) {
		w, _, err := terminal.GetSize(0)
		if err != nil {
			panic(err)
		}

		fmt.Print("\r" + strings.Repeat(" ", w) + "\r" + s)
	}

	go func() {
		for m := range msg {
			slock.RLock()
			if m.Movement != tf2r.NoMovement {
				replaceLine(fmt.Sprintf("%s %s.\n", formatColour(html.UnescapeString(m.Name), m.Color), m.Movement))
			} else {
				replaceLine(fmt.Sprintf("%s: %s\n", formatColour(html.UnescapeString(m.Name), m.Color), replLinks(m.Value)))
			}
			if s != "" {
				fmt.Print(s)
			}
			slock.RUnlock()
		}
	}()

	r := bufio.NewReader(os.Stdin)
	for {
		r, _, err := r.ReadRune()
		if err != nil {
			panic(err)
		}

		if r == '\x1b' {
			return
		}

		slock.Lock()
		s += string(r)
		if r == 13 {
			c.Say(s)
			s = ""
		}

		if r == 127 && len(s) > 1 {
			s = s[:len(s)-2]
			replaceLine(s)
		} else {
			fmt.Print(string(r))
		}

		slock.Unlock()
	}
}
